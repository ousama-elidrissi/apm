import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailGuard } from './guard/product-detail.guard';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routes: Routes = [
  {
    path: '', component: ProductListComponent,
    children: [
      {
        path: ':id', component: ProductDetailComponent
      }
      //, canActivate: [ProductDetailGuard]
      //{ path: '', redirectTo: 'apply', pathMatch: 'full' },
      //{ path: '**', component: Page404leavesComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
